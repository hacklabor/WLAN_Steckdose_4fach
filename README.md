# 4 fach WLAN Steckdose mit NODEMCU

## Schaltplan

![Platine](/Fritzing/NODEMCU_4fach_relais_Steckplatine.png  "")

## Material

![Material](/Bilder/Material.jpg  "")

- ESP8266 (NodeMCU)
- 4 fach Relaisplatine
- Obi Abzweigkasten 150x116
- [https://www.obi.de/installationsdosen/obo-abzweigkasten-aufputz-150-mm-x-116-mm-grau-ip66/p/1801422](https://www.obi.de/installationsdosen/obo-abzweigkasten-aufputz-150-mm-x-116-mm-grau-ip66/p/1801422 "OBI")
- 5V Netzteil
- 3 fach Steckdose "weil billiger als SchukoStecker mit Anschlussleitung"
- 4 Einbaustechsosen 50x50mm
- Aderendhülsen
- Einzelader 1x1mm^2
- Usb Kabel

## Werkzeug

- Akkuschrauber
- Lohkreissäge 45mm
- Lötkolben
- Arduino IDE


## Einbau ca 2,5h

![Einbau1](/Bilder/Einbau1.jpg  "")
Steckdosen einbauen
![Einbau2](/Bilder/Einbau2.jpg  "")
Es passen nur 3 von 4 Schrauben
![Einbau3](/Bilder/Einbau3.jpg  "")
Schutzleiter verbinden
![Einbau4](/Bilder/Einbau4.jpg  "")
Null verbinden
![Einbau6](/Bilder/Einbau6.jpg  "")
Relais L verbinden und Schliesser auf die Steckdosen verdrahten
![Einbau7](/Bilder/Einbau7.jpg  "")
NODEMCU direkt auf Relaisplatine auflöten; Plus und D0-->IN4 mit Einzelader verbinden (verlöten); Pin von NODEMCU abschneiden.
![Einbau8](/Bilder/Einbau8.jpg  "")
Fertig


## Software

Mqtt 

### Steuerung

client.subscribe("inTopic");
Topic "inTopic" 

- 10 = Steckdose1 AUS
- 11 = Steckdose1 EIN
- 20 = Steckdose2 AUS
- 21 = Steckdose2 EIN
- 30 = Steckdose3 AUS
- 31 = Steckdose3 EIN
- 40 = Steckdose4 AUS
- 41 = Steckdose4 EIN

### Rückmeldung

client.publish("outTopic", msg);
Topic "outTopic"
 
- hello world= wdt
- 10 = Steckdose1 AUS
- 11 = Steckdose1 EIN
- 20 = Steckdose2 AUS
- 21 = Steckdose2 EIN
- 30 = Steckdose3 AUS
- 31 = Steckdose3 EIN
- 40 = Steckdose4 AUS
- 41 = Steckdose4 EIN

### !!! Achtung im Repro sind SSID und PW nicht vorhanden!!!!

- const char* ssid = ".........";
- const char* password = "...........";

Bitte einfügen und flashen aber nicht speichern

## Bedienung

über node red
![nodered1](/Bilder/nodered1.png  "")
![nodered2](/Bilder/Nodered2.png  "")




THK
23.04.17



