/*
  Based on Basic ESP8266 MQTT example

  This sketch demonstrates the capabilities of the pubsub library in combination
  with the ESP8266 board/library.

  It connects to an MQTT server then:
  - publishes "hello world" to the topic "outTopic" every two seconds
  - subscribes to the topic "inTopic", printing out any messages
    it receives. NB - it assumes the received payloads are strings not binary
  - If the first character of the topic "inTopic" is an 1, switch ON the ESP Led,
    else switch it off

  It will reconnect to the server if the connection is lost using a blocking
  reconnect function. See the 'mqtt_reconnect_nonblocking' example for how to
  achieve the same result without blocking the main loop.

  To install the ESP8266 board, (using Arduino 1.6.4+):
  - Add the following 3rd party board manager under "File -> Preferences -> Additional Boards Manager URLs":
       http://arduino.esp8266.com/stable/package_esp8266com_index.json
  - Open the "Tools -> Board -> Board Manager" and click install for the ESP8266"
  - Select your ESP8266 in "Tools -> Board"

  Änderung Mqqt "in Topic" = 10 Steckdose1 Aus "in Topic" = 11 Steckdose1 EIN
  Änderung Mqqt "in Topic" = 20 Steckdose2 Aus "in Topic" = 21 Steckdose1 EIN
  usw.
  THK 23.04.17

*/

#include <ESP8266WiFi.h>
#include <PubSubClient.h>
extern "C" {
#include "user_interface.h" //to set the hostname
}


// Update these with values suitable for your network.

const char* ssid = "...";
const char* password = "...";

char* espHostname = "esp-wlansteckdose01";

// MQTT Last will and Testament
byte willQoS = 0;
const char* willTopic = "lwt/esp-wlansteckdose01";
const char* willOnMessage = "online";
const char* willOffMessage = "offline";
const char* willClientID = "esp-wlansteckdose01";
boolean willRetain = true;



const char* mqtt_server = "hautomation01.hacklabor.de";
const int output1 = 14;
const int output2 = 12;
const int output3 = 13;
const int output4 = 5;
WiFiClient espClient;
PubSubClient client(espClient);
long lastMsg = 0;
char msg[50];
int value = 0;

void setup() {
  pinMode(output1, OUTPUT);     // Initialize the BUILTIN_LED pin as an output
  pinMode(output2, OUTPUT);     // Initialize the BUILTIN_LED pin as an output
  pinMode(output3, OUTPUT);     // Initialize the BUILTIN_LED pin as an output
  pinMode(output4, OUTPUT);     // Initialize the BUILTIN_LED pin as an output
  digitalWrite(output1, HIGH);
  digitalWrite(output2, HIGH);
  digitalWrite(output3, HIGH);
  digitalWrite(output4, HIGH);
  Serial.begin(115200);
  setup_wifi();
  client.setServer(mqtt_server, 1883);
  client.setCallback(callback);
}

void setup_wifi() {

  delay(10);
  // We start by connecting to a WiFi network
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);
  wifi_station_set_hostname(espHostname);
  WiFi.mode(WIFI_STA);

  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

void callback(char* topic, byte* payload, unsigned int length) {
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
  }
  Serial.println();

  // Switch on the LED if an 1 was received as first character
  if ((char)payload[0] == '1') {
    if ((char)payload[1] == '1') {
      digitalWrite(output1, LOW);   // Turn the LED on (Note that LOW is the voltage level
      client.publish("outTopic", "11");
      // but actually the LED is on; this is because
      // it is acive low on the ESP-01)
    } else {
      digitalWrite(output1, HIGH);  // Turn the LED off by making the voltage HIGH
      client.publish("outTopic", "10");
    }
  }

  // Switch on the LED if an 2 was received as first character
  if ((char)payload[0] == '2') {
    if ((char)payload[1] == '1') {
      digitalWrite(output2, LOW);   // Turn the LED on (Note that LOW is the voltage level
      client.publish("outTopic", "21");
      // but actually the LED is on; this is because
      // it is acive low on the ESP-01)
    } else {
      digitalWrite(output2, HIGH);  // Turn the LED off by making the voltage HIGH
      client.publish("outTopic", "20");
    }
  }

  // Switch on the LED if an 3 was received as first character
  if ((char)payload[0] == '3') {
    if ((char)payload[1] == '1') {
      digitalWrite(output3, LOW);   // Turn the LED on (Note that LOW is the voltage level
      client.publish("outTopic", "31");
      // but actually the LED is on; this is because
      // it is acive low on the ESP-01)
    } else {
      digitalWrite(output3, HIGH);  // Turn the LED off by making the voltage HIGH
      client.publish("outTopic", "30");
    }
  }

  // Switch on the LED if an 4 was received as first character
  if ((char)payload[0] == '4') {
    if ((char)payload[1] == '1') {
      digitalWrite(output4, LOW);   // Turn the LED on (Note that LOW is the voltage level
      client.publish("outTopic", "41");
      // but actually the LED is on; this is because
      // it is acive low on the ESP-01)
    } else {
      digitalWrite(output4, HIGH);  // Turn the LED off by making the voltage HIGH
      client.publish("outTopic", "40");
    }
  }

}

void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Attempt to connect
    if (client.connect(willClientID, willTopic, willQoS, willRetain, willOffMessage)) {
      Serial.println("connected");
      // Once connected, publish an announcement...
      client.publish(willTopic, willOnMessage, willRetain);      // ... and resubscribe
      client.subscribe("inTopic");
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 500 seconds before retrying
      delay(500);
    }
  }
}
void loop() {

  if (!client.connected()) {
    reconnect();
  }
  client.loop();
}
